export default class ProductService {

    getProductsSmall() {
		return fetch('data/products-small.json').then(res => res.json()).then(d => d.data);
	}

	getProducts() {
		return fetch('data/notif-dummy.json').then(res => res.json()).then(d => d.data);
    }

	getCapa() {
		return fetch('data/capa-dummy.json').then(res => res.json()).then(d => d.data);
    }

	getShipment() {
		return fetch('data/shipment-dummy.json').then(res => res.json()).then(d => d.data);
    }

	getShipment1() {
		return fetch('data/shipment-dummy.json').then(res => res.json()).then(d => d.data1);
    }

	getInvoice() {
		return fetch('data/invoice-dummy.json').then(res => res.json()).then(d => d.inv_data);
    }

	getReceipt() {
		return fetch('data/invoice-dummy.json').then(res => res.json()).then(d => d.receipt_data);
    
	}
	getOpenpo1() {
		return fetch('data/openpo-dummy.json').then(res => res.json()).then(d => d.data);
    }

	getOpenpo2() {
		return fetch('data/openpo-dummy.json').then(res => res.json()).then(d => d.data1);
    }

	getPlanno() {
		return fetch('data/planno-dummy.json').then(res => res.json()).then(d => d.data);
    }

    getProductsWithOrdersSmall() {
		return fetch('data/products-orders-small.json').then(res => res.json()).then(d => d.data);
	}
	
}