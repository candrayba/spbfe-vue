import spbKkt from "./api/SpbKktApi";
class ForgotPasswordService {

    forgotPass(payload) {
        return spbKkt.post('adm/send/forgotpassword', payload);
    }

}
export default new ForgotPasswordService();

