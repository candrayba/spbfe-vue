import spbKkt from "./api/SpbKktApi";
class LoginService {
  async signViaAdminSc(data) {
    try {
        return await spbKkt.post("adm/auth/signviaadminsc", data);
    } catch (e) {
      console.log("Error LoginService.js", e);
    }
  }

}
export default new LoginService();