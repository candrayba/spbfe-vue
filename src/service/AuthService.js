import spbKkt from "./api/SpbKktApi";
class AuthService {
    authKey = false;
    whoAmi() {
        return spbKkt.get('adm/auth/who');
    }

    setAuthKey(key){
        this.authKey = key;
    }

    getAuthKey(){
        return this.authKey;
    }

    async signOutApp(payload) {
        try {
            return await spbKkt.post("adm/auth/signout", payload);
        } catch (e) {
          console.log("Error AuthService.js", e);
        }
      }

    loggedOut(){
        console.log("Logged Out");
        localStorage.clear;
        this.authKey = false;
    }
}
export default new AuthService();

