import { createRouter, createWebHashHistory } from 'vue-router';
// import Dashboard from './components/Dashboard.vue';
import HomePage from './components/HomePage.vue';
import Guard from './app/guard/Guard'

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        beforeEnter(to, from, next) {
            Guard.guardMyroute(to, from, next);
        },
        component: HomePage,
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('./pages/Login.vue')
    },
    {
        path: '/forgotpass',
        name: 'forgotpass',
        component: () => import('./pages/ForgotPassword.vue')
    },
    {
        path: '/error',
        name: 'error',
        component: () => import('./pages/Error.vue')
    },
    {
        path: '/notfound',
        name: 'notfound',
        component: () => import('./pages/NotFound.vue')
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;
