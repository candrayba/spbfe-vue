import AuthService from "../../service/AuthService";

class Guard {
    guardMyroute(to, from, next){
        if(AuthService.getAuthKey()){
            next(); 
        } else {
            next('/login'); 
        }
    }
}

export default new Guard();